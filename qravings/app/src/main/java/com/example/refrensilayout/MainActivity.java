package com.example.refrensilayout;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.refrensilayout.Model.SessionLogin;

public class MainActivity extends AppCompatActivity {

    SessionLogin getlogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //cek Login
        getlogin = new SessionLogin(this);
        //CEK LOGIN
        if(getlogin.getSPSudahLogin()){

        }else{
            Intent main = new Intent(getApplicationContext(),LoginActivity.class);
            startActivity(main);
            finish();
        }

    }

    public void btnGuide(View view) {
        Intent main = new Intent(getApplicationContext(),GuideActivity.class);
        startActivity(main);
    }

    public void btnBest(View view) {
        Intent main = new Intent(getApplicationContext(),BestRecomActivity.class);
        startActivity(main);
    }
    public void btnAddGuide(View view) {
        Intent main = new Intent(getApplicationContext(),addGudeActivity.class);
        startActivity(main);
    }
}
