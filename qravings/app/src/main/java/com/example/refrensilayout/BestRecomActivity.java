package com.example.refrensilayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.refrensilayout.recyler.BestRecyler;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BestRecomActivity extends AppCompatActivity {

    private List<Result> results = new ArrayList<>();
    private BestRecyler viewAdapter;
    private RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_best_recom);

        recyclerView = (RecyclerView)findViewById(R.id.recykerview);
        ButterKnife.bind(this);

        viewAdapter = new BestRecyler(this, results);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(viewAdapter);

        final ProgressBar progressBar = (ProgressBar)findViewById(R.id.progress_bar);


        progressBar.setVisibility(View.VISIBLE);

        String Tanggal = String.valueOf("Scan In");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UtilsApi.BASE_URL_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Register api = retrofit.create(Register.class);
        Call<Value> call = api.warung_list(Tanggal);
        call.enqueue(new Callback<Value>(){
            @Override
            public void onResponse(Call<Value> call, Response<Value> response) {
                String value = response.body().getValue();
                progressBar.setVisibility(View.GONE);
                if (value.equals("1")) {

                    results = response.body().getResult();
                    viewAdapter = new BestRecyler(BestRecomActivity.this, results);
                    recyclerView.setAdapter(viewAdapter);
                }else{

                }
            }

            @Override
            public void onFailure(Call<Value> call, Throwable t) {

                t.printStackTrace();
            }
        });


    }
}
