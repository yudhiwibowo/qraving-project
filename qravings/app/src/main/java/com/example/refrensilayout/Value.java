package com.example.refrensilayout;

import java.util.List;

public class Value {

    String value;
    List<Result> result;
    String judul;
    String deskripsi;
    String penulis;
    String id;

    String nama;
    String alamat;
    String jam;
    String hari;
    String harga;

    public String getNama() {
        return nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public String getHarga() {
        return harga;
    }

    public String getHari() {
        return hari;
    }

    public String getJam() {
        return jam;
    }

    public String getJudul() {
        return judul;
    }

    public String getId() {
        return id;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public String getPenulis() {
        return penulis;
    }

    public String getValue() {
        return value;
    }

    public List<Result> getResult() {
        return result;
    }
}
