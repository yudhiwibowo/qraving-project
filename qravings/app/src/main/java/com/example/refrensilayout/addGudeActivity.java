package com.example.refrensilayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.refrensilayout.Model.ListWarung;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class addGudeActivity extends AppCompatActivity {
    private DatabaseReference mDatabase;
    private EditText edtJudul,edtDesk;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_gude);

        edtJudul = (EditText)findViewById(R.id.tvjudul);
        edtDesk = (EditText)findViewById(R.id.edtDesk);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        RelativeLayout Btnsimpan = (RelativeLayout)findViewById(R.id.btnSimpan);

        Btnsimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String Judul = edtJudul.getText().toString().trim();
                String Desk = edtDesk.getText().toString().trim();
                String Penulis = mDatabase.push().getKey();
                String id=  mDatabase.push().getKey();

                ListWarung listdata = new ListWarung(id,Judul,Penulis,Desk);
                mDatabase.child("Guide").child(id).setValue(listdata).
                        addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                Toast.makeText(addGudeActivity.this, "Notes Added", Toast.LENGTH_SHORT).show();
                                //startActivity(new Intent(getApplicationContext(),HomeScreen.class));
                            }
                        });


            }
        });

    }
}
