package com.example.refrensilayout.recyler;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.refrensilayout.ArtikelActivity;
import com.example.refrensilayout.R;
import com.example.refrensilayout.Result;
import com.example.refrensilayout.WarungActivity;

import java.util.List;


public class BestRecyler extends RecyclerView.Adapter<BestRecyler.ViewHolder> {
    private Context context;
    private List<Result> results;


    public BestRecyler(Context context, List<Result> results) {
        this.context = context;
        this.results = results;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView textView;
        public LinearLayout btn_detail;
        public ViewHolder(View v) {
            super(v);
            textView = (TextView)v.findViewById(R.id.tvjudul);
            btn_detail = (LinearLayout)v.findViewById(R.id.LrList);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_best, parent, false);
        ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Result result = results.get(position);
        holder.textView.setText(result.getNama());

        holder.btn_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent main = new Intent(context.getApplicationContext(), WarungActivity.class);
                main.putExtra(ArtikelActivity.EXTRA_ID, result.getId());
                holder.itemView.getContext().startActivity(main);

            }
        });

    }

    @Override
    public int getItemCount() {
        return results.size();
    }




}
